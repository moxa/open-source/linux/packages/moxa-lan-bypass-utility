# Moxa LAN bypass utility

Moxa LAN bypass utility for V3000 series

## Build
```
$ make MCU_DEVICE=/dev/ttyS2
```
The value of MCU_DEVICE, please refer to [MCU uart port device Name](#MCU-uart-port-device-Name)

## Usage

```
Usage:
        mx-lanbypass-ctl [Options]...
Options:
        -r, --relaymode
                Get relay mode (0:connect|1:disconnect|2:by-pass)
        -r, --relaymode [0|1|2]
                Set relay mode (0:connect|1:disconnect|2:by-pass)
        -w, --wdtresetmode
                Get wdt reset mode (0:reset off|1:reset on)
        -w, --wdtresetmode [0|1]
                Set wdt reset mode (0:reset off|1:reset on)
        -m, --wdtrelaymode
                Get wdt relay mode (0:connect|1:disconnect|2:by-pass)
        -m, --wdtrelaymode [0|1|2]
                Set wdt relay mode (0:connect|1:disconnect|2:by-pass)
        -p, --pwroffrelaymode
                Get power off (S5) relay mode (1:disconnect|1:by-pass)
        -p, --pwroffrelaymode [1|2]
                Set power off (S5) relay mode (1:disconnect|2:by-pass)
```

## MCU uart port device Name
|          | Device Node Name |
|----------|------------------|
| V3000    | /dev/ttyS2       |
| CIM-7100 | /dev/ttyMUE0     |