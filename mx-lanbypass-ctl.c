/*
 * Copyright (C) MOXA Inc. All rights reserved.
 * This software is distributed under the terms of the MOXA SOFTWARE NOTICE.
 * See the file MOXA-SOFTWARE-NOTICE for details.
 *
 * Name:
 *	Moxa LAN Bypass Control Utility
 *
 * Description:
 *	Utility for getting or setting LAN Bypass mode.
 *
 * Authors:
 *	2022	Wayne W Wei
 *	2022	Elvis CW Yao	<ElvisCW.Yao@moxa.com>
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include "mx-lanbypass.h"

void usage()
{
	printf("Usage:\n");
	printf("	mx-lanbypass-ctl [Options]...\n");
	printf("Options:\n");
	printf("	-r, --relaymode\n");
	printf("		Get relay mode (0:connect|1:disconnect|2:by-pass)\n");
	printf("	-r, --relaymode [0|1|2]\n");
	printf("		Set relay mode (0:connect|1:disconnect|2:by-pass)\n");
	printf("	-w, --wdtresetmode\n");
	printf("		Get wdt reset mode (0:reset off|1:reset on)\n");
	printf("	-w, --wdtresetmode [0|1]\n");
	printf("		Set wdt reset mode (0:reset off|1:reset on)\n");
	printf("	-m, --wdtrelaymode\n");
	printf("		Get wdt relay mode (0:connect|1:disconnect|2:by-pass)\n");
	printf("	-m, --wdtrelaymode [0|1|2]\n");
	printf("		Set wdt relay mode (0:connect|1:disconnect|2:by-pass)\n");
	printf("	-p, --pwroffrelaymode\n");
	printf("		Get power off (S5) relay mode (1:disconnect|2:by-pass)\n");
	printf("	-p, --pwroffrelaymode [1|2]\n");
	printf("		Set power off (S5) relay mode (1:disconnect|2:by-pass)\n");
	printf( "\n");
}

int main(int argc, char *argv[])
{
	int c, ret;
	struct option *options;

	struct option long_options[] = {
		{"help", no_argument, 0, 'h'},
		{"relaymode", no_argument, 0, 'r'},
		{"wdtresetmode", no_argument, 0, 'w'},
		{"wdtrelaymode", no_argument, 0, 'm'},
		{"pwroffrelaymode", no_argument, 0, 'p'},
		{0, 0, 0, 0}
	};

	if (argc == 1 || argc > 3) {
		usage();
		return 1;
	}

	c = getopt_long(argc, argv, "hrwmp", long_options, NULL);
	switch (c) {
		case 'h':
			usage();
			exit(0);
		case 'r':
			if (argv[optind]) {
				mx_set_relay_mode(atoi(argv[optind]));
			} else {
				mx_get_relay_mode();
			}
			break;
		case 'w':
			if (argv[optind]) {
				mx_set_wdt_reset_mode(atoi(argv[optind]));
			} else {
				mx_get_wdt_reset_mode();
			}
			break;
		case 'm':
			if (argv[optind]) {
				mx_set_wdt_relay_mode(atoi(argv[optind]));
			} else {
				mx_get_wdt_relay_mode();
			}
			break;
		case 'p':
			if (argv[optind]) {
				mx_set_pwroff_relay_mode(atoi(argv[optind]));
			} else {
				mx_get_pwroff_relay_mode();
			}
			break;
		default:
			usage();
			exit(99);
	}

	return 0;
}
