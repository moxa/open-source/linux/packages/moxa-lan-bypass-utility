/*
 * Copyright (C) MOXA Inc. All rights reserved.
 * This software is distributed under the terms of the MOXA SOFTWARE NOTICE.
 * See the file MOXA-SOFTWARE-NOTICE for details.
 *
 * Name:
 *	Moxa LAN Bypass Control Utility
 *
 * Description:
 *	Utility for getting or setting LAN Bypass mode.
 *
 * Authors:
 *	2022	Wayne W Wei
 *	2022	Elvis CW Yao	<ElvisCW.Yao@moxa.com>
 */

#define HDR_LEN			5
#define DATA_HEAD		7
#define MAX_PKT_LEN		40
#define TIMEOUT			5
#define MAX_SH_SIZE 		128

/* CMD format: chk bit(2) + cmd id(3) + data len(1) + hdr checksum(1) + data(data len) + data checksum(1) */
/* checksum = 0xff - (data sum & 0xff) */
#define RELAY_MODE_HDR		"\x07\xFF\x52\x45\x4C"
#define WDT_RESET_HDR		"\x07\xFF\x57\x52\x53"
#define WDT_RELAY_HDR		"\x07\xFF\x57\x52\x4C"
#define PWROFF_RELAY_HDR	"\x07\xFF\x50\x52\x4C"

void print_cmd(unsigned char *buf, int len);

int mx_set_relay_mode(int relayMode);
int mx_get_relay_mode();

int mx_set_wdt_reset_mode(int resetMode);
int mx_get_wdt_reset_mode();

int mx_set_wdt_relay_mode(int relayMode);
int mx_get_wdt_relay_mode();

int mx_set_pwroff_relay_mode(int relayMode);
int mx_get_pwroff_relay_mode();
